Welcome to Clemmons Trace Village! Here, you receive more than an apartment…you receive a home. You are sure to have room to enjoy peace and tranquility with 2 Bedrooms and 1.5 bath townhome-style apartments. Clemmons Trace Village supports many lifestyles with a warm community atmosphere.

Address: 114 Willow Trace Circle, Suite 7, Clemmons, NC 27012, USA

Phone: 336-245-8383
